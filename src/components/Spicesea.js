import {Card } from 'react-bootstrap'
import {  useNavigate } from 'react-router-dom'

//deconstructed
export default function Spices({spiceProp}){
	let navigate = useNavigate();
	let routeChange = ()=>{
		let path =`/spices/${_id}`;
		navigate(path);
	}

	const {name,description,price, _id} = spiceProp;
	
	return(
			
				
					<Card onClick={routeChange} style={{ cursor: "pointer" }}>
					<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description</Card.Subtitle>
					<Card.Text>
					{description}
					</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text> Php  {price}
					</Card.Text>
					
					{/*<Button variant="primary" as={Link} to ={`/spices/${_id}`}>Details</Button>*/}
				
					</Card.Body>
					</Card>
					
			

		)

}