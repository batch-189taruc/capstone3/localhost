import {Card, Button} from 'react-bootstrap'
/*import { Link } from 'react-router-dom'*/

//deconstructed
export default function mySpices({myspiceProp}){

	const {productName, price, quantity, subtotal, _id} = myspiceProp;
	console.log(myspiceProp)
	return(
			
				
					<Card>
					<Card.Body>
					<Card.Title>{productName}</Card.Title>
					<Card.Subtitle>Spice ID</Card.Subtitle>
					<Card.Text>
					{_id}
					</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text> Php  {price}
					</Card.Text>
					<Card.Subtitle>Quantity:</Card.Subtitle>
					<Card.Text> {quantity}
					</Card.Text>
					<Card.Subtitle>Subtotal:</Card.Subtitle>
					<Card.Text> {subtotal}
					</Card.Text>
					
					{/*<Button variant="primary" as={Link} to ={`/spices/${_id}`}>Details</Button>*/}
				{/*<Link className="btn btn-primary" to="/courseview">Details</Link> this works the same as above*/}
					</Card.Body>
					</Card>
					
			

		)

}