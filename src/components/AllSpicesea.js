import {Card} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'

//deconstructed
export default function AllSpicesea({allSpicesProp}){
	let navigate = useNavigate();
	let routeChange = ()=>{
		let path =`/allspices/${_id}`;
		navigate(path);
	}
	const {_id, name, description, price, stock, isActive, createdOn} = allSpicesProp;
	
	return(
			
				<>
					<Card onClick={routeChange} style={{ cursor: "pointer" }}>
					<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>{_id}</Card.Subtitle>
					<Card.Text>Description: {description}</Card.Text>
					<Card.Text>Price: Php {price}</Card.Text>
					<Card.Text>Stock: {stock}</Card.Text>
					<Card.Text>Active status: {String(isActive)}</Card.Text>
					<Card.Text>Listed date: {createdOn}</Card.Text>
					</Card.Body>
					</Card>
		
					
			</>

		)

}