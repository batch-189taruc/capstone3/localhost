import {Row, Col, Card, Container} from 'react-bootstrap'
import {  useNavigate } from 'react-router-dom'

export default function Highlights(){
  let navigate = useNavigate();
  let curry = ()=>{
    let path =`/spices/62c912c8e630feb5275d63b2`;
    navigate(path);
  }

  let chili = ()=>{
    let path =`/spices/62c9132ae630feb5275d63ba`;
    navigate(path);
  }

   let basil = ()=>{
    let path =`/spices/62dd31cf3a9ae28d03da7a34`;
    navigate(path);
  }
	return(
    <Container className="mt-3 mb-3">
    <h3>Hot Products!</h3>
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" onClick={curry} style={{ cursor: "pointer" }}>
      				<Card.Body>
        				<Card.Title>Curry</Card.Title>
        <Card.Text>
          Curry powder is a mixture of spices with a bright golden hue and a complex flavor. Curry powder may offer a variety of health benefits due to the numerous healthful spices it contains.
        </Card.Text>
      </Card.Body>
    </Card>
			</Col>
				<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" onClick={chili} style={{ cursor: "pointer" }}>
      				<Card.Body>
        				<Card.Title>Chili</Card.Title>
        <Card.Text>
          Boasting high amounts of vitamin C and antioxidants, chillies have been found to help prevent lifestyle diseases including some cancers and stomach ulcers. 
        </Card.Text>
      </Card.Body>
    </Card>
			</Col>
      <Col xs={12} md={4}>
        <Card className="cardHighlight p-3" onClick={basil} style={{ cursor: "pointer" }}>
              <Card.Body>
                <Card.Title>Basil</Card.Title>
        <Card.Text>
          The eugenol in basil can block calcium channels, which may help to lower blood pressure. The essential oils in the herb can help to lower your cholesterol and triglycerides.
        </Card.Text>
      </Card.Body>
    </Card>
      </Col>
		</Row>
    </Container>
		)
}