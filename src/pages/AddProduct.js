import {useState, useContext } from 'react'; //s54 activity
import {Form, Button} from 'react-bootstrap';
import { useNavigate} from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';




export default function AddProduct(){

const {user} = useContext(UserContext)

	const[name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState (0);
	const  [stock, setStock] = useState (0)
	// const  [isActive, setIsActve] = useState (0)

let navigate = useNavigate();
function routeChange1(){
	let path =`/allspices`;
		navigate(path);
}

function addProduct(e){
e.preventDefault()
			 
				fetch("http://localhost:4001/spices/addproduct", {
					method: "POST",
					headers: {
						'Content-type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					stock: stock,
					
					})

				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if (data === true){
						Swal.fire({
							title: "Success!",
							icon: "success",
							text: "New spice added!"
						})
						
						//navigate("/allspices");
					}else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Try again!"
						})
						//navigate("/allspices");
					}
				})
			}


	return(

<Form>
			<h1 className="text-center">Add a new Spice</h1>
			 <Form.Group className="mb-3" controlId="name">
		        <Form.Label>Name</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Enter new name"
		        		value={name}
		        		onChange={e => {setName(e.target.value)}}
		        		required />
		      </Form.Group>
		     
		      <Form.Group className="mb-3" controlId="description">
		        <Form.Label>Description:</Form.Label>
		        <Form.Control 
		        		type="textarea" 
		        		placeholder="Enter new description"
		        		value={description}
		        		onChange={e => {setDescription(e.target.value)}}
		        		required />
		        
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="price">
		        <Form.Label>Price</Form.Label>
		        <Form.Control 
		        		type="number" 
		        		placeholder="Set a new price"
		        		value={price}
		        		onChange={e => {setPrice(e.target.value)}}
		        		required />
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="stock">
		        <Form.Label>Stock Number</Form.Label>
		        <Form.Control 
		        		type="number" 
		        		placeholder="Set a new stock quantity"
		        		value={stock}
		        		onChange={e => {setStock(e.target.value)}}
		        		required />
		      </Form.Group>
		      
		      <Button variant="danger" onClick={() => {routeChange1()}}>Back</Button>
		      <Button variant="primary" onClick={(e) => {addProduct(e);routeChange1()}}>Add</Button>
		    
		      </Form>




		)
}



	



