import {useState, useEffect, useContext } from 'react'; //s54 activity
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
	//State hooks to store the values of the input fields
	const navigate = useNavigate();
	const {user} = useContext(UserContext)
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [address, setAddress] = useState("");
	const [contact, setContact] = useState("");
	
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	//State to determine whether the submitbtn is enabled or not
	const [isActive, setIsActive] = useState(false);
	function registerUser(e){
		
		e.preventDefault()

	fetch("http://localhost:4001/users/checkemail", {
			method: "POST",
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true){
				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})
			} else{
				fetch("http://localhost:4001/users/register", {
					method: "POST",
					headers: {
						'Content-type': 'application/json'
					},
					body: JSON.stringify({
					username: username,
					email: email,
					address: address,
					contact: contact,
					password: password1
					})

				})
				.then(res => res.json())
				.then(data => {
					if (data === true){
						Swal.fire({
							title: "Success!",
							icon: "success",
							text: "You can now buy spices!"
						})
						setUsername("");
						setEmail("");
						setAddress("");
						setContact("");
						setPassword1("");
						setPassword2("");
						navigate("/login");
					}else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Try again!"
						})
					}
				})
			}
		})

		setUsername("");
		setEmail("");
		setAddress("");
		setContact("");
		setPassword1("");
		setPassword2("");
	};
	useEffect(() => {
	if((username !== "" && address !== "" && contact.length===11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
		setIsActive(true)
	}else{
		setIsActive(false)
	}

	}, [username, address, contact, email, password1, password2]);

	
	return(
		(user.id !== null) ? //s54 activity
		<Navigate to="/"/>
		:
		<Form onSubmit={(e)=> registerUser(e)}>
			<h1 className="text-center">Register</h1>
			 <Form.Group className="mb-3" controlId="username">
		        <Form.Label>Username</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Enter your username"
		        		value={username}
		        		onChange={e => {setUsername(e.target.value)}}
		        		required />
		      </Form.Group>
		     
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        		type="email" 
		        		placeholder="Enter email"
		        		value={email}
		        		onChange={e => {setEmail(e.target.value)}}
		        		required />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="address">
		        <Form.Label>Address</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Please provide your address"
		        		value={address}
		        		onChange={e => {setAddress(e.target.value)}}
		        		required />
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="contact">
		        <Form.Label>Contact Number</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Enter contact number"
		        		value={contact}
		        		onChange={e => {setContact(e.target.value)}}
		        		required />
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        		type="password" 
		        		placeholder="Password" 
		        		value={password1}
		        		onChange={e => {setPassword1(e.target.value)}}
		        		required />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        		type="password" 
		        		placeholder="Password"
		        		value={password2} 
		        		onChange={e => {setPassword2(e.target.value)}}
		        		required />
		      </Form.Group>
		      
		      {/*<Button variant="primary" type="submit" id="submitBtn">
		        Submit
		      </Button>*/}

		      {
		      	isActive ?
		      		<Button variant="primary" type="submit" id="submitBtn">
		        	Submit
		      		</Button>:
		      		<Button variant="danger" type="submit" id="submitBtn" disabled>
		        	Submit
		      		</Button>
		      }
		    </Form>



		)

}