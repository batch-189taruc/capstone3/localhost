import {Fragment, useState, useEffect, useContext} from 'react';
import MyOrdersea from '../components/MyOrdersea';
import { Button} from 'react-bootstrap';
// import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext'


export default function MyOrders(){
const {user} = useContext(UserContext)
const [myspices, setMySpices] = useState([])



	useEffect(()=>{
		fetch('http://localhost:4001/users/myorders',{
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			

			
			setMySpices(data.map(myspices =>{
				return(
					<MyOrdersea key={myspices._id} myspiceProp={myspices}/>)
			}))
		})
	}, [])
return(


	<Fragment>
				<h1>My Orders</h1>
				<h5>Delivery to: {user.address}</h5>
				{myspices}
			
			</Fragment>


	)
}
