import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function SpiceView(){
	const {user} = useContext(UserContext)

	//Allow us to gain access to methods that will allow us to redirect to a different page after enrolling our a course
	const navigate = useNavigate(); //useHistory before
	//The "useParams"
	const {spiceId} = useParams();
	const[name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState (0);
	const  [quantity, setQuantity] = useState (1)


	function addQuantity() {
		if (quantity !== 20 ) {
			setQuantity(quantity + 1)
			
		}
	}

	function subtractQuantity() {
		if (quantity !== 0) {
			setQuantity(quantity - 1)
			
		}
	}


	const addSpice = (spiceId) =>{
		fetch(`http://localhost:4001/users/placeorder`, {
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				quantity: quantity,
				subtotal: quantity * price
			})
})
			.then(res=> res.json())
			.then(data => {
				console.log(data);
				if (data === true){
					Swal.fire({
						title: "added to cart",
						icon: "success",
						text: "addedtocart."})
					navigate("/spices")
				}else{
					Swal.fire({
						title: "Nope. Can't do.",
						icon: "error",
						text: "Try again!!!!!"})
				}
			})
		
	}

	useEffect(() => {
		fetch(`http://localhost:4001/spices/${spiceId}`)
		.then(res=> res.json())
		.then(data =>{
			console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [spiceId])

	return(
		<Container className="mt-3">
		<Row>
		<Col lg={{span:6, offset:3}}>
			<Card>
            <Card.Body className="text-center">
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
               	<Card.Subtitle>Quantity:</Card.Subtitle>
               	<span><Button  variant="success" onClick={() => subtractQuantity()} style={{ cursor: "pointer" }}>&#60;</Button>        {quantity}        <Button variant="success" onClick={() => addQuantity()} style={{ cursor: "pointer" }}>&#62;</Button></span>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price * quantity}</Card.Text>
                

                {
                	user.id !== null ?
                	<Button variant="primary" onClick={() => addSpice(spiceId)}>Add</Button>
                	:
                	<Link className="btn btn-danger" to="/login"> Login to purchase</Link>
                }
      
    </Card.Body>
					</Card>
					</Col>
					</Row>
		</Container>

		)
}