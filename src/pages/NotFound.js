import {Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
export default function NotFound(){


	return(
		<Row>
			<Col className="p-5">
				<h1>Page Not Found</h1>
				<p>Go back to homepage.</p>
				<Button variant="primary" type="submit" id="submitBtn" as={Link} to="/">Home</Button>
			</Col>
		</Row>


		)
}