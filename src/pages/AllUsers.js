import {Fragment, useEffect, useState} from 'react';

import AllUsersea from '../components/AllUsersea';
export default function AllUsers() {

	const [users, setUsers] = useState([])
	

	useEffect(() => {
		fetch('http://localhost:4001/users/allusers',{
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUsers(data.map(userAll =>{

				return (
					<AllUsersea key={userAll._id} allUsersProp={userAll}/>

					)

			}))
		})


	}, [])

	return(

			
			<Fragment>
				<h1>All Registered Users</h1>
				{users}
			</Fragment>
		)


}