import { navigate, useNavigate} from 'react-router-dom';
import {Card,  Button} from 'react-bootstrap'

export default function Banner(){

let navigate = useNavigate();
function routeChange(){
	let path =`/`;
		navigate(path);
}

return(

			<Card>
      				<Card.Body>
        				<Card.Title style={{textAlign: "center"}} >About this site</Card.Title>
        <Card.Text style={{textAlign: "center"}}>
          		This is my submission for the capstone 3 requirement in <a href="https://zuitt.co/">Zuitt Coding bootcamp </a>. I will modify this capstone to add more features and function as I learn to solve coding problems that I impose to myself and this site.

          		All the functions, pages, and codes have met the minimum requirement for a working e-commerce website. This site have accomplished the following:</Card.Text>

 <Card.Text>1. Registration Page 
   - A Registration Page with proper form validation (all fields must be filled and passwords must match) 
   that must redirect the user to the login page once successfully submitted.</Card.Text>
<Card.Text>2. Login Page
   - A Login Page that must redirect the user to the either the home page or the products catalog 
   once successfully authenticated</Card.Text>
<Card.Text>3. Product Catalog  </Card.Text>
 <Card.Text>  - A Product Catalog page that must show all products currently available (unavailable/inactive products must not be shown) 
   and allow users to visit individual product pages</Card.Text>
<Card.Text>4. Specific Product Page</Card.Text>
   <Card.Text>- A Specific Product page that must show all relevant product information, with a feature to allow only logged-in users to 
   add any quantity of the product to their cart or make an order</Card.Text>
<Card.Text>5. Order Page</Card.Text>
  <Card.Text> - An Order History page where the currently logged-in user can see all records of their own previously-placed orders</Card.Text>
<Card.Text>6. Admin/Dashboard</Card.Text>
   <Card.Text>- An Admin Panel/Dashboard with the following features:</Card.Text>
      <Card.Text>- Retrieve list of all products (available or unavailable)</Card.Text>
      <Card.Text>- Create new product</Card.Text>
      <Card.Text>- Update product information</Card.Text>
      <Card.Text>- Deactivate/reactivate product</Card.Text>
<Card.Text>7. Other :</Card.Text>
 <Card.Text>  - A fully-functioning Navbar with proper dynamic rendering (Register/Login links for users not logged in, 
    Logout link for users who are, etc)</Card.Text>
  <Card.Text>  - App must be single-page and utilize proper routing (no navigating to another page/reloading)</Card.Text>
   <Card.Text> - Registration/Login pages must be inaccessible to users who are logged-in</Card.Text>
   <Card.Text> - Apart from users who are not logged-in, Admin must not be able to add products to their cart</Card.Text>
   <Card.Text> - Do not create a website other than the required e-commerce app</Card.Text>
 <Card.Text>   - Do not use templates found in other sites</Card.Text>
<Card.Text>  - Full responsiveness across mobile/tablet/desktop screen sizes (not 100% completed)</Card.Text>
 <Card.Text> - A hot products/featured products section</Card.Text>
 <Card.Text> - Admin feature to retrieve a list of all orders made by all users</Card.Text>
 <Card.Text> - Cart View</Card.Text>
   <Card.Text>  A Cart View page with the following features:</Card.Text>
  <Card.Text>    - Show all items the user has added to their cart (and their quantities)</Card.Text>
    <Card.Text>  - Remove products from cart</Card.Text>
    <Card.Text>  - Subtotal for each item</Card.Text>
      
      
      
   
  <Card.Text> Please feel free to use the credentials below:</Card.Text>

   <Card.Text>Admin User:</Card.Text>
   <Card.Text> -- email: admin@email.com</Card.Text>
   <Card.Text> -- password: admin123</Card.Text>
  <Card.Text> Dummy Customer</Card.Text>
   <Card.Text> -- email: customer@mail.com</Card.Text>
   <Card.Text> -- password: customer123</Card.Text>

        
        <Button variant="primary" onClick ={() => {routeChange()}}>Home</Button>
      </Card.Body>
    </Card>

	)

}