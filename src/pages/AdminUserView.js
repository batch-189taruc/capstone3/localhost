import {useState, useEffect, useContext} from 'react';
import { Card, Button} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext'



export default function AdminUserView(){
	const {user} = useContext(UserContext)

	//Allow us to gain access to methods that will allow us to redirect to a different page after enrolling our a course
	
	//The "useParams"
	const {userId} = useParams();
	const [username, setUsername] = useState("");
	const [email, setEmail] = useState("");
	const [address, setAddress] = useState("");
	const [contact, setContact] = useState("");
	const [isAdmin, setIsAdmin] = useState("");
	

function deleteuserbtn(){
	fetch(`http://localhost:4001/users/deleteuser/${userId}`,{
			method: "DELETE",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res=> alert('user has been deleted'))
}
function updateAsAdminbtn(){
	fetch(`http://localhost:4001/users/updateuser1/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res=> alert(`${username} is now an admin!`))
}
function removeAsAdminbtn(){
	fetch(`http://localhost:4001/updateuser2/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}).then(res=> alert(`${username}'s admin status has been removed!`))
}

let navigate = useNavigate();

function routeChange(){
	let path =`/allusers`;
		navigate(path);
}

	useEffect(() => {
		fetch(`http://localhost:4001/users/allusers/${userId}`,{
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			console.log(data)
			setUsername(data.username)
			setEmail(data.email)
			setAddress(data.address)
			setContact(data.contact)
			setIsAdmin(data.isAdmin)
		})
	}, [userId])

	return(
		
			<Card>
					<Card.Body>
					<Card.Title>{username}</Card.Title>
					<Card.Subtitle>{userId}</Card.Subtitle>
					<Card.Text>email: {email}</Card.Text>
					<Card.Text>Contact Number: {contact}</Card.Text>
					<Card.Text>address: {address}</Card.Text>
					<Card.Text>Admin tag: {String(isAdmin)}</Card.Text>
					<Card.Text>password: "null"</Card.Text>
					
					{
						(isAdmin === false) ?
						<Button variant="success" onClick ={() => {updateAsAdminbtn(); routeChange()}}>Update as admin</Button>
						:
						<Button variant="secondary" onClick ={() => {removeAsAdminbtn(); routeChange()}}>Remove as admin</Button>
									}
					<Button variant="danger" onClick ={() => {deleteuserbtn(); routeChange()}}>Delete User</Button>
					</Card.Body>
					</Card>


		)
}