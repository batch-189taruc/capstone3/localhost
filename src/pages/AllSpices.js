import {Fragment, useEffect, useState} from 'react';
import {Button} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

import AllSpicesea from '../components/AllSpicesea';
export default function AllSpices() {
	const [spices, setSpices] = useState([])

let navigate = useNavigate()
function routetoAdd(){
	let path1 =`/addproduct`;
		navigate(path1);
}

	useEffect(() => {
		fetch('http://localhost:4001/users/allspices',{
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setSpices(data.map(spicesAll =>{

				return (
					<AllSpicesea key={spicesAll.name} allSpicesProp={spicesAll}/>

					)

			}))
		})


	}, [])

	return(

			
			<Fragment>
				<h1>All Listed Spices</h1>
				<Button variant="success" onClick={() => {routetoAdd()}}>Add/Create</Button>
				{spices}
			</Fragment>
		)


}